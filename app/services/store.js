import Service from '@ember/service';
import Option from 'ember-quiz-app/models/option';
import Poll from 'ember-quiz-app/models/poll';

const polls = [];

/* first poll */
let poll = Poll.create({
	id: '1',
	options: [],
	prompt: 'What is a group of Antelope called?',
	votes: []
});

poll.get('options').pushObjects([
	Option.create({ id: '1', label: 'Troop', poll: poll}),
	Option.create({ id: '2', label: 'Herd', poll: poll}),
	Option.create({ id: '3', label: 'Colony', poll: poll})
]);

polls.pushObjects(poll);

/* second poll */
poll = Poll.create({
	id: '2',
	options: [],
	prompt: 'What is a female Aligator called?',
	votes: []
});

poll.get('options').pushObjects([
	Option.create({ id: '4', label: 'Doe', poll: poll}),
	Option.create({ id: '5', label: 'Sow', poll: poll}),
	Option.create({ id: '6', label: 'Queen', poll: poll})
]);

polls.pushObjects(poll);

export default Service.extend({
	findAllPolls() {
		return polls;
	},
	findPoll(id) {
		return this.findAllPolls().findBy('id', id);
	}
});
