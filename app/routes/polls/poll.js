import Route from '@ember/routing/route';
// import Ember from 'ember';
// import { inject as service } from '@ember/service';

export default Route.extend({
	model(params) {
		return this.get('store').findPoll(params.poll_id);
	},
	store: Ember.inject.service()
	// store: service
});
